<?php
    
    class View
    {
        public $viewVars;
        public $controller;
        public $action;
        
        function __construct($controller, $action)
        {
            $this->controller = $controller;
            $this->action = $action;
        }
        
        public function loadHelpers($helpers = [])
        {
            foreach($helpers as $helper) {
                $file = HELPERS . DS . $helper . 'Helper.php';
                if(is_file($file)) {
                    require $file;
                    $class = $helper . 'Helper';
                    $this->$helper = new $class;
                } else {
                    echo "Missing Helper: $file";
                }
            }
        }
        
        public function render()
        {
            $file = LAYOUTS . DS . 'default.ctp';
            if(is_file($file)) {
                extract($this->viewVars);
                require $file;
            } else {
                echo "Missing View: $file";
            }
        }
        
        public function content_for_layout()
        {
            $file = VIEWS . DS . $this->controller . DS . $this->action . '.ctp';
            if(is_file($file)) {
                extract($this->viewVars);
                require $file;
            } else {
                echo "Missing View: $file";
            }
        }
        
        public function element($name, $vars = [])
        {
            $file = ELEMENTS . DS . $name . '.ctp';
            if(is_file($file)) {
                extract($vars);
                require $file;
            } else {
                echo "Missing Element: $file";
            }
        }
    }