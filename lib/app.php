<?php
    
    class App
    {
        static function uses($path, $file)
        {
            $ext = '.php';
            if($path == ELEMENTS || $path == VIEWS || $path == LAYOUTS) {
                $ext = '.ctp';
            }
            $file = $path . DS . $file . $ext;
            if(is_file($file)) {
                require $file;
                
                return true;
            }
            echo "Missing File: $file";
            
            return false;
        }
        
        static function loadMethod($controller, $method, $params)
        {
            if(method_exists($controller, $method)) {
                call_user_func_array([$controller, $method], $params);
                
                return true;
            }
            echo "Missing Method: {$controller->controller}Controller::{$method}()";
            
            return false;
        }
    }