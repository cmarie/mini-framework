<?php
    
    class Configure
    {
        private static $settings;
        
        static function all()
        {
            return Self::$settings;
        }
        
        static function write($key, $value)
        {
            if(strstr($key, '.')) {
                list($k, $v) = explode('.', $key);
                self::$settings[$k][$v] = $value;
            } else {
                self::$settings[$key] = $value;
            }
        }
        
        static function read($key = null)
        {
            if($key) {
                if(strstr($key, '.')) {
                    list($k, $v) = explode('.', $key);
                    
                    return self::$settings[$k][$v];
                } else {
                    return self::$settings[$key];
                }
            } else {
                return self::$settings;
            }
        }
    }