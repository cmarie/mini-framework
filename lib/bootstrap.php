<?php
    require "app.php";
    App::uses(LIB, 'configure');
    App::uses(CONFIG, 'core');
    App::uses(LIB, 'globals');
    App::uses(LIB, 'controller');
    App::uses(CONTROLLERS, 'AppController');
    App::uses(LIB, 'view');
    App::uses(LIB, 'helper');
    App::uses(HELPERS, 'AppHelper');
    App::uses(LIB, 'component');
    App::uses(COMPONENTS, 'AppComponent');
    App::uses(LIB, 'model');
    App::uses(MODELS, 'AppModel');
    
    class Bootstrap extends App
    {
        private $controller = 'Home';
        private $action = 'index';
        private $params = [];
        
        function __construct()
        {
            session_start(Configure::read('Session'));
            // Parse URL
            if(Configure::read('Rewrite.method') == 'GET') {
                $path = isset($_GET['p']) ? $_GET['p'] : '';
            } else {
                $path = env('REQUEST_URI');
            }
            $url = array_filter(explode('/', $path));
            // Controller
            if(!empty($url[1])) {
                $this->controller = ucwords($url[1]);
            }
            // Action
            if(!empty($url[2])) {
                $this->action = strtolower($url[2]);
            }
            // Params
            for($i = 3; $i <= count($url); $i++) {
                $this->params[] = $url[$i];
            }
            // Load Controller if exists
            if($this->uses(CONTROLLERS, $this->controller . 'Controller')) {
                $class = $this->controller . 'Controller';
                $controller = new $class;
                $controller->loadComponents();
                $controller->setController($this->controller);
                $controller->setAction($this->action);
                $controller->loadModel();
                // Check if action/method exists inside of the controller
                if($this->loadMethod($controller, $this->action, $this->params)) {
                    $controller->loadView();
                }
            }
        }
    }