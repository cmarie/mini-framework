<?php
    
    class Controller
    {
        public $controller;
        public $action;
        public $viewVars = [];
        public $helpers = [];
        public $components = [];
        public $data = [];
        
        function __construct()
        {
            if(!empty($_POST['data'])) {
                $this->data = $_POST['data'];
            }
        }
        
        public function loadModel()
        {
            // Load Model
            if(App::uses(MODELS, $this->controller)) {
                $model = new $this->controller();
                $model->data = $this->data;
                $this->{$this->controller} = $model;
                $model->beforeFilter();
                $this->data = $model->data;
            }
        }
        
        public function loadView()
        {
            $this->view = new View($this->controller, $this->action);
            $this->view->viewVars = $this->viewVars;
            $this->view->loadHelpers($this->helpers);
            $this->view->render();
        }
        
        public function loadComponents()
        {
            foreach($this->components as $component) {
                if(App::uses(COMPONENTS, $component . 'Component')) {
                    $class = $component . 'Component';
                    $this->$component = new $class;
                }
            }
        }
        
        public function setController($controller)
        {
            $this->controller = $controller;
        }
        
        public function setAction($action)
        {
            $this->action = $action;
        }
        
        public function set($arr = [])
        {
            $this->viewVars = $arr;
        }
        
        public function redirect($url = 'index.php')
        {
            if(is_array($url)) {
                if(Configure::read('Rewrite.method') == 'GET') {
                    header('Location: ?p=/' . implode('/', $url));
                } else {
                    header('Location: ' . implode('/', $url));
                }
            } else {
                header('Location: ' . $url);
            }
            exit();
        }
    }