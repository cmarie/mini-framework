<?php
    function debug($a)
    {
        if(Configure::read('App.debug')) {
            echo sprintf("<pre>%s</pre>", print_r($a, true));
        }
    }
    
    function env($key = null)
    {
        if($key) {
            return $_SERVER[$key];
        }
        
        return $_SERVER;
    }