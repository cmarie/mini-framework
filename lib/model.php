<?php
    App::uses(DATASOURCES, "MySQL");
    
    class Model extends MySQL
    {
        public $data;
        
        public function beforeFilter()
        {
        }
        
        public function beforeSave($data = [])
        {
            return true;
        }
        
        public function beforeFind($data = [])
        {
            return true;
        }
        
        public function find($type = 'all', $conditions = [])
        {
            if($this->beforeFind($conditions)) {
                return parent::find($type, $conditions);
            }
            
            return false;
        }
        
        public function read($fields = [], $id = null)
        {
            if($this->beforeFind($conditions)) {
                return parent::read($fields, $id);
            }
            
            return false;
        }
        
        public function save($data = [])
        {
            if($this->beforeSave($data)) {
                return parent::save($data);
            }
            
            return false;
        }
        
        public function saveField($data = [])
        {
            if($this->beforeSave($data)) {
                return saveField($data);
            }
            
            return false;
        }
        
        public function delete($id)
        {
            return parent::delete($id);
        }
    }