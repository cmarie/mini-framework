<?php
    define('DS', '/');
    define('ROOT', DS . 'var' . DS . 'www' . DS . 'carlymarie.net' . DS . 'domains' . DS . 'dev');
    define('APP', ROOT . DS . 'app');
    define('CONFIG', APP . DS . 'config');
    define('LIB', ROOT . DS . 'lib');
    define('VIEWS', APP . DS . 'views');
    define('LAYOUTS', VIEWS . DS . 'layouts');
    define('ELEMENTS', VIEWS . DS . 'elements');
    define('MODELS', APP . DS . 'models');
    define('DATASOURCES', MODELS . DS . 'datasources');
    define('CONTROLLERS', APP . DS . 'controllers');
    define('COMPONENTS', CONTROLLERS . DS . 'components');
    define('HELPERS', VIEWS . DS . 'helpers');
    require LIB . DS . 'bootstrap.php';
    $app = new Bootstrap();