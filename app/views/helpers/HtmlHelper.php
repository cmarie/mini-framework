<?php
    
    class HtmlHelper extends Helper
    {
        public function link($text = null, $url = [], $options = [])
        {
            debug(implode('/', array_values($url)));
            if(is_array($url)) {
                $url = '?p=/' . implode('/', array_values($url));
            }
            $options['href'] = $url;
            
            return $this->tag('a', $text, $options);
        }
        
        public function url($url = [])
        {
            if(Configure::read('Rewrite.method') == 'GET') {
                return '?p=/' . implode('/', array_values($url));
            } else {
                return implode('/', array_values($url));
            }
        }
        
        public function tag($el = '', $value = '', $options = [])
        {
            $attribs = '';
            foreach($options as $attr => $val) {
                $attribs .= sprintf('%s="%s" ', $attr, $val);
            }
            $attribs = trim($attribs);
            if($el == 'link') {
                return sprintf("<%s%s/>", $el, $attribs ? ' ' . $attribs : '');
            } else {
                return sprintf("<%s%s>%s</%s>", $el, $attribs ? ' ' . $attribs : '', $value, $el);
            }
        }
        
        public function css($files = [])
        {
            $callback = '';
            foreach($files as $file) {
                $callback .= $this->tag('link', '', [
                    'href' => Configure::read('Asset.base_css') . DS . $file . '.css' . (Configure::read('Asset.timestamps') ? '?' . time() : null),
                    'rel' => 'stylesheet'
                ]);
            }
            
            return $callback;
        }
        
        public function script($files = [])
        {
            $callback = '';
            foreach($files as $file) {
                $callback .= $this->tag('script', '', ['src' => Configure::read('Asset.base_js') . DS . $file . '.js' . (Configure::read('Asset.timestamps') ? '?' . time() : null)]);
            }
            
            return $callback;
        }
    }