<?php
    
    class AppController extends Controller
    {
        public $helpers = ['Html'];
        public $components = ['Auth', 'Session'];
    }