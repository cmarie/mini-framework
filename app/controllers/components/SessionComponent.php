<?php
    
    class SessionComponent extends AppComponent
    {
        static function setFlash($msg, $class = 'error')
        {
            $_SESSION['flash']['message'] = $msg;
            $_SESSION['flash']['class'] = $class;
        }
        
        static function getFlash()
        {
            $callback = '';
            if(!empty($_SESSION['flash'])) {
                $callback = sprintf('<div class="%s">%s</div>', $_SESSION['flash']['class'], $_SESSION['flash']['message']);
                unset($_SESSION['flash']);
            }
            
            return $callback;
        }
    }