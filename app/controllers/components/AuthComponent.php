<?php
    
    class AuthComponent extends AppComponent
    {
        static function login($user)
        {
            $_SESSION['auth']['User'] = $user;
            return true;
        }
        
        static function user($key = null)
        {
            if(!empty($_SESSION['auth']['User'])) {
                if($key) {
                    return $_SESSION['auth']['User'][$key];
                }
                
                return $_SESSION['auth']['User'];
            }
            
            return false;
        }
    
        static function logout()
        {
            if(isset($_SESSION['auth']['User'])) {
                unset($_SESSION['auth']['User']);
            }
        }
    }