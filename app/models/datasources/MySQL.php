<?php
    
    class MySQL
    {
        public $id;
        public $table;
        public $schema;
        
        function __construct()
        {
            $this->__schema();
        }
        
        public function find($type = 'all', $conditions = [])
        {
            $callback = false;
            $conn = $this->__connect();
            if($conn) {
                $options = '';
                foreach($conditions as $field => $value) {
                    $options .= sprintf('`%s` = "%s" AND ', $field, $value);
                }
                if(!empty($options)) {
                    $options = substr($options, 0, strlen($options) - 5);
                    $sql = sprintf("SELECT * FROM `%s` WHERE %s", $this->table, $options);
                } else {
                    $sql = sprintf("SELECT * FROM `%s`", $this->table);
                }
                if($type == 'all') {
                    $callback = $conn->query($sql)->fetch_all(MYSQLI_ASSOC);
                } elseif($type == 'first') {
                    $callback = $conn->query($sql)->fetch_assoc();
                }
                $conn->close();
            }
            
            return $callback;
        }
        
        public function read($fields = [], $id = null)
        {
            $callback = false;
            $conn = $this->__connect();
            if($conn) {
                if(!$fields) {
                    $fields = '*';
                } else {
                    $fields = implode(',', $fields);
                }
                $sql = sprintf("SELECT %s FROM `%s` WHERE `id` = %s", $fields, $this->table, $id);
                $callback = $conn->query($sql)->fetch_assoc();
                $conn->close();
            }
            
            return $callback;
        }
        
        public function save($data = [])
        {
            $callback = false;
            $conn = $this->__connect();
            if($conn) {
                list($sql, $insert) = $this->__parseData($data);
                if($conn->query($sql) === true) {
                    if($insert) {
                        $this->id = $conn->insert_id;
                    }
                    $callback = true;
                }
                $conn->close();
            }
            
            return $callback;
        }
        
        public function saveField($data = [])
        {
            $callback = false;
            $conn = $this->__connect();
            if($conn) {
                $sql = $this->__parseData($data);
                $callback = $conn->query($sql)->fetch_assoc();
                $conn->close();
            }
            
            return $callback;
        }
        
        public function delete($id)
        {
            $callback = false;
            $conn = $this->__connect();
            if($conn) {
                $sql = sprintf("DELETE FROM `%s` WHERE `id` = %s", $this->table, $id);
                if($conn->query($sql) === true) {
                    $callback = true;
                }
                $conn->close();
            }
            
            return $callback;
        }
        
        private function __schema()
        {
            $conn = $this->__connect();
            if($conn) {
                $sql = sprintf("SHOW COLUMNS FROM %s", $this->table);
                $fields = [];
                foreach($conn->query($sql)->fetch_all() as $column) {
                    $field = $column[0];
                    $value = trim($column[1]);
                    preg_match('!\(([^\)]+)\)!', $value, $match);
                    $fields[$field]['type'] = explode('(', $value)[0];
                    $fields[$field]['length'] = @$match[1];
                }
                $this->schema = $fields;
                $conn->close();
            }
        }
        
        private function __connect()
        {
            if($this->table) {
                return new mysqli(Configure::read('Database.hostname'), Configure::read('Database.username'), Configure::read('Database.password'), Configure::read('Database.database'));
            }
            
            return false;
        }
        
        private function __parseData($_data = [])
        {
            $_data = (isset($_data[$this->table]) ? $_data[$this->table] : $_data);
            $is_insert = isset($_data['id']);
            $this->id = isset($_data['id']) ? $_data['id'] : $this->id;
            if($this->id) {
                $_data['id'] = $this->id;
            }
            if(array_key_exists('user_id', $this->schema) && empty($_data['user_id'])) {
                $_data['user_id'] = $_SESSION['User']['id'];
            }
            if($is_insert) {
                $_data['created'] = date('Y-m-d H:i:s');
            }
            $_data['modified'] = date('Y-m-d H:i:s');
            if($is_insert) {
                $columns = implode("`, `", array_keys($_data));
                $values = [];
                foreach(array_values($_data) as $value) {
                    $values[] = addslashes($value);
                }
                $values = implode("', '", $values);
                $sql = sprintf("INSERT INTO `%s` (`%s`) VALUES ('%s')", $this->table, $columns, $values);
            } else {
                $fields = '';
                foreach($_data as $field => $value) {
                    $fields .= sprintf("`%s` = '%s', ", $field, addslashes($value));
                }
                $fields = substr($fields, 0, strlen($fields) - 2);
                $sql = sprintf("UPDATE `%s` SET %s WHERE `id` = %s", $this->table, $fields, $_data['id']);
            }
            
            return [$sql, $is_insert];
        }
    }